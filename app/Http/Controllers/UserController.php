<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function getData() 
    {        
        $data = User::all();        
        return view('welcome', compact('data'));
    }   

    public function create()
    {                      
        return view('create');             
    } 

    public function store(Request $request)
    {   
        $request->validate([
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'patronymic' => 'required|string|max:255',
            'phone' => 'required|integer',
        ]);              
        User::create($request->all());
        return redirect()->route('index');             
    }    

    public function edit(User $user)
    {
        return view('edit', compact('user'));
    } 

    public function destroy(User $user)
    {
        $user->delete();
        return redirect()->route('index');
    } 

    public function update(Request $request, User $user)
    {
        $user->update($request->all());
        return redirect()->route('index');            
    }
}


