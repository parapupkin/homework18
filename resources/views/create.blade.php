@extends('layouts.app')
@section('container')
    <h2>Введите данные новой записи</h2>
    <form action="{{ route('store') }}" method="post">
        {{ csrf_field() }}
        @if ($errors->has('name') || $errors->has('surname') || $errors->has('patronymic') || $errors->has('phone'))                  
        <span class="errors">Заполните все поля формы корректными значениями</span><br>                        
        @endif  
        Фамилия
        <input class="input_create" type="text" name="surname"><br>
        Имя
        <input class="input_create" type="text" name="name"><br>                
        Отчество
        <input class="input_create" type="text" name="patronymic"><br>
        Телефон
        <input class="input_create" type="text" name="phone"><br>
        <input class="input_create" type="hidden" name="id"><br>
        <input class="input_create" type="submit" value="добавить запись">
    </form>       
@endsection 