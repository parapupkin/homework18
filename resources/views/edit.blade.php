@extends('layouts.app')
@section('container')
    <h2>Пожалуйста, отредактируйте нужные Вам данные</h2>
    <form action="{{ route('update', $user) }}" method="post">
        {{ csrf_field() }}
        Фамилия
        <input class="input_create" type="text" name="surname" value="{{ $user->surname }}"><br>
        Имя
        <input class="input_create" class="input_create" type="text" name="name" value="{{ $user->name }}"><br>     
        Отчество
        <input class="input_create" type="text" name="patronymic" value="{{ $user->patronymic }}"><br>
        Телефон
        <input class="input_create" type="text" name="phone" value="{{ $user->phone }}"><br>
        <input class="input_create" type="hidden" name="id" value="{{ $user->id }}"><br>
        <input class="input_create" type="submit" value="обновить запись">
    </form>       
@endsection 