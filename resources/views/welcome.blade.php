@extends('layouts.app')
@section('container')
    <h1>Привет любителям laravel!</h1>
    <p>Ниже создан список, который просили в домашке</p>               
    <table class="item">
        <tr>
            <th>№п/п</th>
            <th>Фамилия</th>
            <th>Имя</th>
            <th>Отчество</th>
            <th>Телефон</th> 
            <th>Редактировать</th> 
            <th>Удалить</th>              
        </tr>
        @foreach ($data as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->surname }}</td>
            <td>{{ $item->name }}</td>
            <td>{{ $item->patronymic }}</td>
            <td>{{ $item->phone }}</td>
            <td>
                <form action="{{ route('edit', $item) }}" method="post">
                    {{ csrf_field() }}
                    <input type="submit" value="Редактировать">
                </form>                        
            </td>
            <td>                
                <form action="{{ route('delete', $item) }}" method="post">                    
                    {{ csrf_field() }}                                        
                    <input type="submit" value="Удалить">
                </form>                        
            </td>
        </tr>
        @endforeach            
        <hr>            
    </table>
    <a href="{{ route('create') }}">Добавить контакт в таблицу</a>   
@endsection         
       