<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \DB::table('users')->insert([
            'id' => 1,
            'surname' => 'Пупкин',
            'name' => 'Василий',
            'patronymic' => 'Леонидович',
            'phone' => '778899'
        ]);      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \DB::table('users')->where('id', '=', 1)->delete();
    }
}
