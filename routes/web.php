<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
    // return view('welcome');

// });

Route::get('/', 'UserController@getData')->name('index');

Route::get('/create', 'UserController@create')->name('create');

Route::post('/destroy/{user}', 'UserController@destroy')->name('delete');

Route::post('/edit/{user}', 'UserController@edit')->name('edit');

Route::post('/update/{user}', 'UserController@update')->name('update');

Route::post('/store', 'UserController@store')->name('store');




